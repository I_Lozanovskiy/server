package com.example.FinalToDo.services;

import com.example.FinalToDo.configs.IdNotFound;
import com.example.FinalToDo.models.GetResp;
import com.example.FinalToDo.models.GetRespPag;
import com.example.FinalToDo.models.ToDoTask;
import com.example.FinalToDo.repo.ToDoRepoz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import java.util.List;



@org.springframework.stereotype.Service
public class ServiceApp {

    @Autowired
    ToDoRepoz toDoRepoz;

    public ToDoTask create(ToDoTask task) {
        return ToDoTask.toDTO(toDoRepoz.save(ToDoTask.fromDTO(task)));
    }

    public GetRespPag getPaginated (Integer page, Integer perPage, Boolean status) {

        Page<ToDoTask> tasks = (status != null) ?
                toDoRepoz.findByStatusIs(status, PageRequest.of(page-1, perPage, Sort.by(Sort.Direction.DESC, "id")))
                        .map(ToDoTask::toDTO) :
                toDoRepoz.findAll(PageRequest.of(page-1, perPage, Sort.by(Sort.Direction.DESC, "id")))
                        .map(ToDoTask::toDTO);
        return new GetRespPag(tasks.getContent(),
                toDoRepoz.countByStatus(false),
                (int) tasks.getTotalElements(),
                toDoRepoz.countByStatus(true));
    }
//    public GetResp Paginated (int page, int perPage) {
//        var tasks = toDoRepoz.findAllSorted()
//                .stream()
//                .map(ToDoTask::toDTO)
//                .collect(Collectors.toList());
//        return getResponse(tasks, page, perPage);
//    }
//
//    public GetResp PaginatedSorted(int page, int perPage, Boolean status) {
//        var tasks = toDoRepoz.findByStatus(status)
//                .stream()
//                .map(ToDoTask::toDTO)
//                .collect(Collectors.toList());
//        return getResponse(tasks, page, perPage);
//    }

    private GetResp getResponse(List<ToDoTask> originalTasks, Integer page, Integer perPage) {
        List<ToDoTask> anotherTasks;
        try {
            anotherTasks = originalTasks.subList(perPage * (page-1), perPage * (page-1) + perPage);
        }
        catch (IndexOutOfBoundsException e) {
            anotherTasks = originalTasks.subList(perPage * (page-1), originalTasks.size());
        }
        return GetResp.builder()
                .content(anotherTasks)
                .ready((int) anotherTasks.stream().filter(ToDoTask::isStatus).count())
                .notReady((int) anotherTasks.stream().filter(x -> !x.isStatus()).count())
                .numberOfElements(anotherTasks.size())
                .build();
    }

    public void deleteAllReady() {
        toDoRepoz.deleteWhereReady();
    }

    public void patch(boolean status) {
        toDoRepoz.changeEveryStatus(status);
    }

    public void deleteOne(int id) {
        toDoRepoz.deleteById(id);
    }

    public void patchStatus(int id, boolean status) {
        if (toDoRepoz.existsById(id)) {
            toDoRepoz.changeOneStatus(status, id);
        }
        else
            throw new IdNotFound("Non correct id");
    }

    public void patchText(int id, String text) {
        if (toDoRepoz.existsById(id)){
            toDoRepoz.changeOneText(text, id);
        }
        else
            throw new IdNotFound("Non correct id");

    }
}
