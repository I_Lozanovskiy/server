package com.example.FinalToDo.configs;

public class IdNotFound extends RuntimeException{
    public IdNotFound(String message){
        super(message);
    }
}

