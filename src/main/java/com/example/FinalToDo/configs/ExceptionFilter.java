package com.example.FinalToDo.configs;


import com.example.FinalToDo.configs.IdNotFound;
import com.example.FinalToDo.ErrorCode;
import com.example.FinalToDo.models.ErrorResp;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.List;




@ControllerAdvice
public class ExceptionFilter {
    private ResponseEntity handleException(String message, List<Integer> codesList) {
        return new ResponseEntity<>(
                ErrorResp.builder()
                        .statusCode(ErrorCode.getCode(message))
                        .codes(codesList)
                        .success(true)
                        //.timeStamp(Timestamp.from(Instant.now()))
                        .build(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    protected ResponseEntity<ErrorResp> handleValidationException(ConstraintViolationException ex) {
        List<Integer> codesList = ex.getConstraintViolations().stream()
                .map(x -> ErrorCode.getCode(x.getMessage())).collect(Collectors.toList());
        String msg = ex.getConstraintViolations().stream().findFirst().get().getMessage();
        return handleException(msg, codesList);
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    protected ResponseEntity<ErrorResp> handleMissedParameterException(MissingServletRequestParameterException ex) {
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode(ex.getMessage()));
        String msg = ex.getMessage();
        return handleException(msg, codesList);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    protected ResponseEntity<ErrorResp> handlePostValidation(MethodArgumentNotValidException ex) {
        String msg = ex.getFieldErrors().get(0).getDefaultMessage();
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode(msg));
        return handleException(msg, codesList);
    }

    @ExceptionHandler(value = EmptyResultDataAccessException.class)

    protected ResponseEntity<ErrorResp> handleNonExistingId(EmptyResultDataAccessException ex) {
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode("task not found"));
        return handleException("task not found", codesList);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<ErrorResp> handleTypeMismatch(MethodArgumentTypeMismatchException ex) {
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode("Http request not valid"));
        return handleException("Http request not valid", codesList);
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    protected ResponseEntity<ErrorResp> handleMessageNotReadable(HttpMessageNotReadableException ex) {
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode("Http request not valid"));
        return handleException("Http request not valid", codesList);
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    protected ResponseEntity<ErrorResp> handleNotSupportedRequest(HttpRequestMethodNotSupportedException ex) {
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode("this request doesn't exist for this address"));
        return handleException("this request doesn't exist for this address", codesList);
    }

    @ExceptionHandler(value = IdNotFound.class)
    protected ResponseEntity<ErrorResp> handleNonExistinId(IdNotFound ex) {
        List<Integer> codesList = Collections.singletonList(ErrorCode.getCode("task not found"));
        return handleException("task not found", codesList);
    }
}