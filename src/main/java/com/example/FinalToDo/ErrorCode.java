package com.example.FinalToDo;

public enum ErrorCode {
    UNKNOWN(0, "unknown"),
    USERNAME_SIZE_NOT_VALID(1, "username size not valid"),
    ROLE_SIZE_NOT_VALID(2, "role size not valid"),
    EMAIL_SIZE_NOT_VALID(3, "email size not valid"),
    MUST_NOT_BE_NULL(4, "must not be null"),
    USER_NOT_FOUND(5, "Could not find user"),
    TOKEN_NOT_PROVIDED(6, "JWT token not provided"),
    UNAUTHORISED(7, "unauthorised"),
    USER_EMAIL_NOT_NULL(8, "user email mustn't be null"),
    USER_PASSWORD_NULL(9, "user password must be null"),
    USER_ROLE_NOT_NULL(10, "user role mustn't be null"),
    NEWS_DESCRIPTION_SIZE(11, "size of news description not valid"),
    NEWS_DESCRIPTION_NOT_NULL(12, "news description has to be valid"),
    NEWS_TITLE_SIZE(13, "news title size not valid"),
    NEWS_TITLE_NOT_NULL(14, "title has to be present"),
    PARAM_PAGE_NOT_NULL(15, "Required Integer parameter 'page' is not present"),
    PARAM_PER_PAGE_NOT_NULL(16, "Required Integer parameter 'perPage' is not present"),
    USER_EMAIL_NOT_VALID(17, "user email must be a well-formed email address"),
    PAGE_SIZE_NOT_VALID(18, "news page must be greater or equal 1"),
    PER_PAGE_MIN_NOT_VALID(19, "perPage must be greater or equal 1"),
    PER_PAGE_MAX_NOT_VALID(19, "perPage must be less or equal 100"),
    CODE_NOT_NULL(20, "Required String parameter 'code' is not present"),
    EXCEPTION_HANDLER_NOT_PROVIDED(21, "Exception handler not provided"),
    REQUEST_IS_NOT_MULTIPART(22, "Current request is not a multipart request"),
    MAX_UPLOAD_SIZE_EXCEEDED(23, "Maximum upload size exceeded"),
    USER_AVATAR_NOT_NULL(24, "user avatar mustn't be null"),
    PASSWORD_NOT_VALID(25, "password not valid"),
    PASSWORD_NOT_NULL(26, "user password mustn't be null"),
    NEWS_NOT_FOUND(27, "news not found"),
    ID_MUST_BE_POSITIVE(29, "id must be positive"),
    USER_ALREADY_EXISTS(30, "User already exists"),
    TODO_TEXT_NOT_NULL(31, "text must not be null"),
    TODO_TEXT_SIZE_NOT_VALID(32, "text size of todo not valid"),
    TODO_STATUS_NOT_NULL(33, "todo status must not be null"),
    TASK_NOT_FOUND(34, "task not found"),
    TASK_PATCH_UPDATED_NOT_CORRECT_COUNT(35, "task patch updated not correct count"),
    TASKS_PAGE_GREATER_OR_EQUAL_1(37, "tasks page must be greater or equal 1"),
    TASKS_PER_PAGE_GREATER_OR_EQUAL_1(38, "tasks per page must be greater or equal 1"),
    TASKS_PER_PAGE_LESS_OR_EQUAL_100(39, "tasks per page must be less or equal 100"),
    REQUIRED_INT_PARAM_PAGE_IS_NOT_PRESENT(40, "Required request parameter 'page' for method parameter type Integer is not present"),
    REQUIRED_INT_PARAM_PER_PAGE_IS_NOT_PRESENT(41, "Required request parameter 'perPage' for method parameter type Integer is not present"),
    USER_NAME_HAS_TO_BE_PRESENT(43, "username has to be present"),
    TAGS_NOT_VALID(44, "tags not valid"),
    NEWS_IMAGE_HAS_TO_BE_PRESENT(45, "news image has to be present"),
    USER_WITH_THIS_EMAIL_ALREADY_EXIST(46, "user with this email already exists"),
    HTTP_MESSAGE_NOT_READABLE_EXCEPTION(47, "Http request not valid");

    private final int code;
    private final String message;
    private ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public static int getCode(String message) {
        int code = 0;
        for (ErrorCode e: ErrorCode.values()
        ) {
            if (message.equals(e.message)) {
                code = e.code;
                break;
            }
        }
        return code;
    }
}
