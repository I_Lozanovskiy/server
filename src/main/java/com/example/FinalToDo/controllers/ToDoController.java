package com.example.FinalToDo.controllers;


import com.example.FinalToDo.models.*;
import com.example.FinalToDo.services.ServiceApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@CrossOrigin
@RestController
@Validated
@RequestMapping("/api/v1/todo")
public class ToDoController {

    @Autowired
    ServiceApp serviceApp;
    private final BasicEmptyResp successResponse = new BasicEmptyResp (1,true);
    @PostMapping
    public ResponseEntity<BaseResp<ToDoTask>> create(@Validated @RequestBody ToDoTask task){
        BaseResp<ToDoTask> basicResp = new BaseResp<>(serviceApp.create(task), 1, true);
        return new ResponseEntity<>(basicResp, HttpStatus.OK);

    }


    @GetMapping
    public ResponseEntity<BaseResp<GetRespPag>> getPaginated(@RequestParam  @Min(value = 1 ) int page  ,
                                                             @RequestParam  @Min(value = 1) @Max(value = 10) int perPage,
                                                             @RequestParam(required = false) Boolean status) {

        GetRespPag resp = serviceApp.getPaginated(page, perPage, status);
        BaseResp<GetRespPag> formedResp = new BaseResp<>(resp, 1, true);
        return new ResponseEntity<>(formedResp, HttpStatus.OK);
    }

    @DeleteMapping
        public ResponseEntity<BasicEmptyResp> deleteAllReady() {
            serviceApp.deleteAllReady();
            return new ResponseEntity<>(successResponse, HttpStatus.OK);
        }

    @PatchMapping
    public ResponseEntity<BasicEmptyResp> patch(@RequestBody PatchSwitch request) {
            serviceApp.patch(request.isStatus());
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BasicEmptyResp> deleteOne(@PathVariable("id") int id) {
            serviceApp.deleteOne(id);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @PatchMapping("/status/{id}")
    public ResponseEntity<BasicEmptyResp> patchStatus(@PathVariable("id") int id, @RequestBody PatchSwitch request){
            serviceApp.patchStatus(id, request.isStatus());
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @PatchMapping("/text/{id}")
    public ResponseEntity<BasicEmptyResp> patchText(@PathVariable("id") int id, @RequestBody PatchText request){
            serviceApp.patchText(id, request.getText());
            return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }




}
