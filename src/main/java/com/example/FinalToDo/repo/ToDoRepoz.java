package com.example.FinalToDo.repo;

import com.example.FinalToDo.entities.TaskEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ToDoRepoz extends JpaRepository<TaskEntity, Integer> {
    Integer countByStatus(Boolean status);
    @Query("select t from TaskEntity t where t.status = :status order by t.updatedAt DESC")
    List<TaskEntity> findByStatus(@Param("status") boolean status);
    @Query("select t from TaskEntity t order by t.updatedAt DESC")
    List<TaskEntity> findAllSorted();
    @Modifying
    @Query("update TaskEntity t set t.text=:text where t.id=:id")
    void changeOneText(@Param("text") String text, @Param("id") int id);
    @Modifying
    @Query("delete from TaskEntity t where t.status = true")
    void deleteWhereReady();
    @Modifying
    @Query("update TaskEntity t set t.status=:status")
    void changeEveryStatus(@Param("status") boolean status);
    @Modifying
    @Query("update TaskEntity t set t.status=:status where t.id=:id")
    void changeOneStatus(@Param("status") boolean status, @Param("id") int id);

    @Query(value = "SELECT * FROM todos WHERE status = ?1",
            countQuery = "SELECT count(*) FROM todos WHERE status = ?1",
            nativeQuery = true)
    Page<TaskEntity> findByStatusIs(Boolean status, PageRequest var1);


}
