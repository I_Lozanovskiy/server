package com.example.FinalToDo.models;

import lombok.Data;

@Data
public class PatchSwitch {
    private boolean status;
}
