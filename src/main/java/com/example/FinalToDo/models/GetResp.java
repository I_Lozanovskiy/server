package com.example.FinalToDo.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class GetResp {

    private final List<?> content;
    private final int notReady;
    private final int numberOfElements;
    private final int ready;

}
