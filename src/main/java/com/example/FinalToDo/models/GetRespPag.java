package com.example.FinalToDo.models;

;
import lombok.Data;

import java.util.List;
@Data
public class GetRespPag {

    private final List<?> content;
    private final int notReady;
    private final int numberOfElements;
    private final int ready;
    }

