package com.example.FinalToDo.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class BaseResp<T> {
    private final T data;
    private final int statusCode;
    private final boolean success;


}
