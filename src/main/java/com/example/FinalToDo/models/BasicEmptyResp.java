package com.example.FinalToDo.models;

import lombok.Data;

@Data
public class BasicEmptyResp {
    private final int statusCode;
    private final boolean success;
}
