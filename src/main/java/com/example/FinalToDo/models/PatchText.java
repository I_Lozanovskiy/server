package com.example.FinalToDo.models;

import lombok.Data;

@Data
public class PatchText {
    String text;
}
