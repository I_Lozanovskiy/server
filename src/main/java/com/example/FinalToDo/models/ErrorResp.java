package com.example.FinalToDo.models;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.sql.Timestamp;
import java.util.List;

@Data
@Builder

public class ErrorResp {

    private final int statusCode;
    private final boolean success;
    private List<Integer> codes;
    @CreationTimestamp
    private Timestamp createdAt;
}
