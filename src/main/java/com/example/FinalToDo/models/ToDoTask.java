package com.example.FinalToDo.models;


import com.example.FinalToDo.entities.TaskEntity;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Optional;


public class ToDoTask {
    //private Optional<Integer> id;
    @Setter @Getter private Integer id;

    @NotBlank
    @Size(min = 3, message="enter correct task")
    //private Optional<String> text;
    @Setter @Getter private String text;
    @Setter @Getter @NotNull private boolean status;
    @Setter @Getter private Timestamp createdAt;
    @Setter @Getter private Timestamp updatedAt;

    public static ToDoTask toDTO(TaskEntity taskEntity) {
        ToDoTask toDoTask = new ToDoTask();
        toDoTask.setCreatedAt(taskEntity.getCreatedAt());
        toDoTask.setId(taskEntity.getId());
        toDoTask.setStatus(taskEntity.isStatus());
        toDoTask.setText(taskEntity.getText());
        toDoTask.setUpdatedAt(taskEntity.getUpdatedAt());
        return toDoTask;
    }

    public static TaskEntity fromDTO(ToDoTask toDoTask) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setText(toDoTask.getText());
        taskEntity.setStatus(toDoTask.isStatus());
        return taskEntity;
    }
}
