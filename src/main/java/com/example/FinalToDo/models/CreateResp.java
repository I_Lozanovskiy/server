package com.example.FinalToDo.models;


import lombok.Data;

import java.sql.Timestamp;
@Data
public class CreateResp {

    private Timestamp createdAt;
    private Integer id;
    private boolean status;
    private String text;
    private Timestamp updatedAt;




}
//"data": {
//        "createdAt": "2021-06-15T13:57:35.068Z",
//        "id": 0,
//        "status": true,
//        "text": "string",
//        "updatedAt": "2021-06-15T13:57:35.068Z"
//        },
//        "statusCode": 0,
//        "success": true